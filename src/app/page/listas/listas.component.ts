import { Component, OnInit } from '@angular/core';
import { EstudiantesService } from 'src/app/services/estudiantes.service';

@Component({
  selector: 'app-listas',
  templateUrl: './listas.component.html',
  styleUrls: ['./listas.component.scss']
})
export class ListasComponent implements OnInit {
  estudiantesList:any = [];

  constructor(
    private _estudiantSrv:EstudiantesService
  ) { }

  ngOnInit(): void {
    this._estudiantSrv.getEstudiantes().subscribe((res:any)=> {
      this.estudiantesList = res.content;
      console.log(res);
    })
  }

}
