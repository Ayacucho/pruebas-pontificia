import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  title = 'LISTA DE ALUMNOS';
  nombre:string;
  rol = "ADMIN";
  data= [
    {
        "idpersona": 1,
        "nombre": "Laura Thalia",
        "apellido": "Mazuelo De la Torre",
        "edad": 25,
        "dni": "48484848",
        "fechaNacimiento": "1997-01-21T00:00:00.000+00:00",
        "genero": "F",
        "codigo": "27272727",
        "serie": "500",
        "escuela": {
            "idescuela": 3,
            "denominacion": "Ingeniería de sistemas",
            "codigoEscuela": "50"
        }
    },
    {
        "idpersona": 2,
        "nombre": "Pablo",
        "apellido": "Sanchez Marquina",
        "edad": 30,
        "dni": "25252525",
        "fechaNacimiento": "1992-09-21T00:00:00.000+00:00",
        "genero": "M",
        "codigo": "27272727",
        "serie": "500",
        "escuela": {
            "idescuela": 3,
            "denominacion": "Ingeniería de sistemas",
            "codigoEscuela": "50"
        }
    },
    {
        "idpersona": 3,
        "nombre": "Miguel",
        "apellido": "Araujo Torres",
        "edad": 30,
        "dni": "99999999",
        "fechaNacimiento": "1992-09-21T00:00:00.000+00:00",
        "genero": "M",
        "codigo": "26262626",
        "serie": "500",
        "escuela": {
            "idescuela": 3,
            "denominacion": "Ingeniería de sistemas",
            "codigoEscuela": "50"
        }
    },
    {
        "idpersona": 5,
        "nombre": "Diana",
        "apellido": "Vivanco Flores",
        "edad": 27,
        "dni": "47474747",
        "fechaNacimiento": "1995-09-21T00:00:00.000+00:00",
        "genero": "F",
        "codigo": "26262629",
        "serie": "500",
        "escuela": {
            "idescuela": 3,
            "denominacion": "Ingeniería de sistemas",
            "codigoEscuela": "50"
        }
    },
    {
        "idpersona": 6,
        "nombre": "Josselin Thalia",
        "apellido": "Fernandez Araujo",
        "edad": 27,
        "dni": "47474740",
        "fechaNacimiento": "1995-09-21T00:00:00.000+00:00",
        "genero": "F",
        "codigo": "26262611",
        "serie": "500",
        "escuela": {
            "idescuela": 3,
            "denominacion": "Ingeniería de sistemas",
            "codigoEscuela": "50"
        }
    },
    {
        "idpersona": 7,
        "nombre": "Juan",
        "apellido": "Peralta Morales",
        "edad": 29,
        "dni": "47474709",
        "fechaNacimiento": "1995-09-21T00:00:00.000+00:00",
        "genero": "M",
        "codigo": "26262699",
        "serie": "500",
        "escuela": {
            "idescuela": 3,
            "denominacion": "Ingeniería de sistemas",
            "codigoEscuela": "50"
        }
    }
]
constructor() {
    console.log('constructor');  
    this.nombre = 'FELIX ASTO BERROCAL'
}
ngOnInit(): void {
    console.log('ngOnInit');
    const edad = 32;
    console.log(edad);
    
}
ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    
    console.log('ngAfterViewInit');
}
ngonDestroy(): void {
    
}
}
