import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL_SERVICIOS } from 'src/environments/environment';
import { Observable } from 'rxjs';
var token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNjU0MDM4MzExLCJleHAiOjE2NTQ2NDMxMTF9.35DuWMY7xFjYFE_sJmtahMD2p4CoQq4FeO6oH1WyqxZiRk1LDEmX0RYcMTvKEJAK6Fhc-wxzeJN9GVU7Ze8qsg'

@Injectable({
  providedIn: 'root'
})
export class EstudiantesService {
  api:string = URL_SERVICIOS;
  constructor(
    private http: HttpClient,
  ) { }

  public getEstudiantes(pagina=0, idescuela=1):Observable<any> {
    return this.http.get<any>(this.api + `/estudiante/listPorPagina?pagina=${pagina}&idescuela=${idescuela}`,{ headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token }) });
  }
}
